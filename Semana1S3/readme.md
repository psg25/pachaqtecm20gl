# Primera semana - Sesión 3

Cómo instalar [GIT](https://git-scm.com/) en tu PC:
[Aquí](https://www.youtube.com/watch?v=ES2xtLyI-B8) un video de referencia 

## Sistemas de Control de Versiones
Existen los locales, centrales y distribuidos

- **SCV Locales:** El método de elección de control de versión de muchas personas es copiar archivos en otro directorio (quizás un directorio con marca de tiempo, si son inteligentes). Este enfoque es muy común porque es muy simple, pero también es increíblemente propenso a errores.
- **SCV Centralizados:** Para facilitar la colaboración de múltiples desarrolladores en un solo proyecto los sistemas de control de versiones evolucionaron: en vez de almacenar los cambios y versiones en el disco duro de los desarrolladores, estos se almacenaban en un servidor.
- **SCV Distribuidos:** Le da a cada desarrollador una copia local de todo el proyecto, de esta manera se construye una red distribuida de repositorios, en la que cada desarrollador puede trabajar de manera aislada pero teniendo un mecanismo de resolución de conflictos mucho más elegante que un su versión anterior.