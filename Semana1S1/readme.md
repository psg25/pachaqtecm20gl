# Primera Semana - Sesión 1

Empezamos nuestro curso de Backend **Backend**
## Qué es el Backend?
El backend es la parte del desarrollo web que se encarga de que toda la lógica de una página web funcione. Se trata del conjunto de acciones que pasan en una web pero que no vemos como, por ejemplo, la comunicación con el servidor.

![BackendvsFrontend](https://res.cloudinary.com/practicaldev/image/fetch/s--QFWF9cBP--/c_limit,f_auto,fl_progressive,q_auto,w_880/https://blog.back4app.com/wp-content/uploads/2019/07/make-app-backend-frontend.png)