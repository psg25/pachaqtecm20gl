# Configurando GitHub y GitLab

1. Crear cuenta en GitHub/GitLab
2. Configurar cuenta
3. Crear nuevo proyecto (documento que inicie con archivo Readme)
4. Clonar el repositorio copiando el link HTTPS (Se copia la URL)
5. Ingresar al GIT Bash
6. Localizar carpeta donde se va a trabajar (Repositorio)
8. Ingresar el comando git clone y tu URL por ej. git https://github.com/tuusuario/turespositorio
9. Entrar a carpeta de archivos y abrir repositorio con clic derecho en VS Code
10. Ya esta listo tu respositorio, todas las actualizaciones se van a ver en tu proyecto en GitHub/GitLab. Solo no te olvides de guardar los cambios en tu archivo, realizar los commit respectivos y ejecutar el comando git push en tu terminal para actualizar tu trabajo en GitHub/GitLab